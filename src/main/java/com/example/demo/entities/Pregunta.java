package com.example.demo.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pregunta")
public class Pregunta {

	

	private Long id;


	private String cont_pregunta;
	
	
	private Set<Respuesta> respuesta = new HashSet<Respuesta>(0);
	
	public Pregunta() {

	}
	
	
	public Pregunta(Long id, String cont_pregunta, Set<Respuesta> respuesta) {
		this.id = id;
		this.cont_pregunta = cont_pregunta;
		this.respuesta = respuesta;
	}



	// getters y setters
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pregunta")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/** El cont_pregunta. */
	@Column(name = "cont_pregunta")
	public String getCont_pregunta() {
		return cont_pregunta;
	}

	public void setCont_pregunta(String cont_pregunta) {
		this.cont_pregunta = cont_pregunta;
	}
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pregunta")
	public Set<Respuesta> getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(Set<Respuesta> respuesta) {
		this.respuesta = respuesta;
	}

	@Override
	public String toString() {
		return "Pregunta [id=" + id + ", cont_pregunta=" + cont_pregunta + ", respuesta=" + respuesta + "]";
	}

	
	
}
