package com.example.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "respuesta")
public class Respuesta {
	
	
	private Long id;
	

	private Pregunta pregunta;
	

	private String cont_respuesta;
	
	public Respuesta() {
		
		
	}

	public Respuesta(Long id, Pregunta pregunta, String cont_respuesta) {
		this.id = id;
		this.pregunta = pregunta;
		this.cont_respuesta = cont_respuesta;
	}

	/** El id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_respuesta")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_pregunta", nullable = false)
	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

	/** El cont_pregunta. */
	@Column(name = "cont_respuesta")
	public String getCont_respuesta() {
		return cont_respuesta;
	}

	public void setCont_respuesta(String cont_respuesta) {
		this.cont_respuesta = cont_respuesta;
	}

	@Override
	public String toString() {
		return "Respuesta [id=" + id + ", pregunta=" + pregunta + ", cont_respuesta=" + cont_respuesta + "]";
	}
	
	
	

}
