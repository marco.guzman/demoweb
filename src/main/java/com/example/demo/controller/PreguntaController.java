package com.example.demo.controller;

import java.lang.ProcessBuilder.Redirect;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.entities.Pregunta;
import com.example.demo.repository.PreguntaRepository;
import com.example.demo.service.PreguntaService;



@Controller
public class PreguntaController {

	@GetMapping("/greeting")
	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
		model.addAttribute("name", name);
		return "greeting";
	}
	
	
	@Autowired 
	private PreguntaService preguntaService;
	
	@RequestMapping("/")
	public String vistaListPregunta(Model model) {
		List<Pregunta> preguntaList = preguntaService.listAll();
		model.addAttribute("getAllPregunta", preguntaList);
		return "listarEncuesta";
	}
	
	
	@RequestMapping("/new_add")
	public String vistaNuevaPreguntaForm(Model model) {
		Pregunta pregunta = new Pregunta();
		model.addAttribute("pregunta", pregunta);
		return "insert";
	}
	
	
	@RequestMapping(value = "/save_pregunta", method = RequestMethod.POST)
	public String addNuevoPregunta(@ModelAttribute("pregunta") Pregunta pregunta) {
		preguntaService.create(pregunta);
		return "redirect:/";
	}

}