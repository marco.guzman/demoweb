package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.demo.entities.Pregunta;


public interface PreguntaRepository extends JpaRepository<Pregunta, Long>{
	
	
}
