package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Pregunta;
import com.example.demo.repository.PreguntaRepository;

@Service
public class PreguntaService {

@Autowired
private PreguntaRepository  preguntaRepository;



public List<Pregunta> listAll(){
	
	return preguntaRepository.findAll();
}

public void create(Pregunta pregunta) {
	
	preguntaRepository.save(pregunta);
}

}
